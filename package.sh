#!/bin/bash -e

version=0.0.0

# Setup environment for building inside Dockerized toolchain
#[ $(id -u) = 0 ] && umask 0

#rm -rf *.tgz *.sha256sum lib package SHA256SUMS
#pip3 install -r requirements.txt -t lib --no-binary :all: --prefix ""
#rm -rf *.tgz *.sha256sum package SHA256SUMS
#mkdir -p package
#cp -r pkg LICENSE manifest.json *.py README.md package/
#cd package
cd data/addons/ew2023-wt-demo
rm SHA256SUMS
find . -type f -name '*.pyc' -delete
find . -type d -empty -delete
find . -type f \! -name SHA256SUMS -exec shasum --algorithm 256 {} \; >> SHA256SUMS
cat SHA256SUMS
#cd ..
#TARFILE="ew2023-wt-demo-${version}.tgz"
#tar czf ${TARFILE} package
#shasum --algorithm 256 ${TARFILE} > ${TARFILE}.sha256sum
#cat ${TARFILE}.sha256sum

