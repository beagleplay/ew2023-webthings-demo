#!/bin/bash

rm -rf wt-gw-env
python3 -m virtualenv -p python3 wt-gw-env
source wt-gw-env/bin/activate
python -m pip install -r requirements.txt
python setup.py install

docker pull webthingsio/gateway
docker container rm webthings-gateway
docker container create \
    -e TZ=America/New_York \
    -v $PWD/data:/home/node/.webthings \
    --network="host" \
    --log-opt max-size=1m \
    --log-opt max-file=10 \
    --name webthings-gateway \
    webthingsio/gateway:latest
