import gateway_addon
import threading
import time
import socket
import sys
import struct

class _udpListener():
    # https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(_udpListener, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

    def __init__(self):
        self.sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(('', 9999))
        self.sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_LOOP, True)
        self.mreq = struct.pack("16s15s".encode('utf-8'), socket.inet_pton(socket.AF_INET6, "ff02::1"), (chr(0) * 16).encode('utf-8'))
        self.sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_JOIN_GROUP, self.mreq)

    def listen(self):
        return self.sock.recvfrom(1024)

    def print(self):
        sender, data = self.sock.recvfrom(1024)
        print (str(sender) + '  ' + repr(data))
        return sender, data

#class udpListener(metaclass=_udpListener):
#    pass

class Sensortest(gateway_addon.Device):
    """sensortest device type"""

    def __init__(self):
        """
        Initialize the object (???)
        """

    def watchAddress(self, address):
        return

    def discover(self, timeout):
        values = {}
        return values

    def GetDevice(self, _id, device):
        return
